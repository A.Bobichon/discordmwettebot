const Discord = require('discord.js');
const ytdl = require('ytdl-core');
const path = require('path');
const fs = require('fs');
const bot = new Discord.Client();
const prefix = '#';

const pathMp3Folder = path.join(__dirname, 'mp3');
const BotID = "702163484040888380";

let links = [];

function JoinAndPlay(entity, link){ 
    
    const voice = entity.member.voice.channel;

    voice ? voice.join() 
            .then( connection => {
                //let dispatcher = connection.play(ytdl('https://www.youtube.com/watch?v=rj2WmCAluq4', { filter: 'audioonly' }));
                let dispatcher = connection.play(link);

                dispatcher.on('finish', () => {
                    connection.disconnect();
                });
            }) 
        : console.log("Error whit the channel");
    return;
};

// TEST //
bot.once('ready', ()=> {
    console.log('Ready!');
    /* TEST CHANNEL NAME */

    fs.readdir(pathMp3Folder, (err, file)=>{
        
        if(err){
            return console.log("can't scan the directory: " + err);
        }

        file.forEach((file)=>{
            links.push(`mp3/${file}`);
        })

    });
});

// ACTION ON MESSAGE
bot.on( 'message', message => {

    let commande = message.content.substring(1);
    
    // Do something if the commande start by "#"
    if(message.content.startsWith(prefix)){
        links.forEach((file)=>{
            
            let fileCommande = file.split(".")[0];
            if(commande === fileCommande.split('/')[1]) JoinAndPlay(message, file);
        });
    }
});

bot.login('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
